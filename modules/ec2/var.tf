variable "machinecount" {
  type = number
  default = 1
}
variable "sshkey"{
    type = string
    default = "test-key"
}

variable "nameprefix"{
    type = string
    default = "default"
}