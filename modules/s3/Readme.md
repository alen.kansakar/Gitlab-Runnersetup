# s3

AWS S3 bucket

## Usage

```terraform
module "s3-main" {
  source = "../s3"

  name = "s3-main"

  common_tags  = tags
  account_id   = data.aws_caller_identity.current.id
  account_name = Accnme
}
```
