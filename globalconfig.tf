provider "aws" {
  region  = "us-west-2"
}

locals {
  subnet_id     = "subnet-0a277b00e33a60e6a"
  publickeypair = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC+8yS3U4tzrH+l/lecLWl8e93EVKPADihoU2dFUiN4XyppsNKzxSwNZfHexCK+JWCq4CaMSbSy6CO/q64V3nKdDxAbq93bJA/4DMye/+YhQE1065ueMmuIa4S1bhKXripZMXAYXasHKJBEPY8h8Ln6cuYRfTYsbaDgfPqHeC751x5uKfUGkgn7n94tzs4Xndbq83t6BTyiNlDhg4ktgQvqBIUhJOP3DXWNvQiA0ShFaTbVNMjJB9KspTMsdAahjusHngDV5e1X7/rg+KofMSqqc83XrXv27Fpkn6Q8MZE7W1ET+S1yNYcVOBXuNyWfnH2Fe69xLBVrHsMpOzzUvdvawxorXKVMRP7LN3m6bGct6uOgdYnOWxj7tcRfjskgpkYie7PetzVhmB08RfmqVFpTVdWC0lYXbKfH055dxCGS6JQCPvwZrSHhL8lwJXshLcg5gdAEfzpOSPFN02gG1wsX5FoSsKHU6zJ0nwsxWliNxox1/BCLLhhK11RrcO67ymU= alen.kansakar@AMB-P38XMD6T"
  common_tags = {
    BillingCode = "test"
  }
  ssh_security_group = "sg-0c8b7011a594758fa"
}