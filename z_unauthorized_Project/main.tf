module "Newproject" {
    source = "../modules/ec2"
    machinecount = 1
}

resource "aws_sns_topic" "user_updates" {
  name = "user-updates-topic"
}