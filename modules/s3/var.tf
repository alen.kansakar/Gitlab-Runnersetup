variable "account_id" {
  type = string
}

variable "account_name" {
  description = "account name (uuid for tenants, string for lower environments)"
  type        = string
}

variable "common_tags" {
  type = map(string)
}

variable "name" {
  description = "S3 bucket name"
  type = string
}

variable "policy" {
  description = "S3 bucket policy, set attach_policy to true"
  default = null
  type = string
}

variable "attach_policy" {
  description = "attach S3 bucket policy bool"
  default = false
  type = bool
}

variable "force_destroy" {
  description = "force destroy"
  default = false
  type = bool
}

variable "kms_policy" {
  default = null
  type = string
}
