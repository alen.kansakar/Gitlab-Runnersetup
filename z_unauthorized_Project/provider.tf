terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket = "mystatebucket022294756946"
    key    = "tfstate/project99"
    region = "us-west-2"
  }
}