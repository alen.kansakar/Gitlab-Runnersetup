locals {
  tf-module = "s3"
  tags      = merge(
  var.common_tags,
  {
    component = var.name
    tf-module = local.tf-module
    Name      = var.name
  }
  )
}

module "s3_bucket" {

  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "2.9.0"

  bucket = var.name
  acl    = "private"

  attach_policy = var.attach_policy
  policy        = var.policy

  block_public_acls                    = true
  block_public_policy                  = true
  ignore_public_acls                   = true
  restrict_public_buckets              = true
  versioning                           = {
    enabled = true
  }
  tags                                 = local.tags
  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        kms_master_key_id = aws_kms_key.cmk.id
        sse_algorithm     = "aws:kms"
      }
    }
  }
  force_destroy = var.force_destroy
}
