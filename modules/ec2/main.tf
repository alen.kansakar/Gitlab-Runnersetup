resource "aws_instance" "example" {
  count                  = var.machinecount
  ami                    = "ami-0b28dfc7adc325ef4"
  instance_type          = "t3.micro"
  subnet_id              = "subnet-09303c01171a501ba"
  vpc_security_group_ids = ["sg-0c8b7011a594758fa"]
  key_name               = var.sshkey
  root_block_device {
    volume_size = 10
  }
  tags = {
    Name = "${var.nameprefix}-${count.index}"
  }
  lifecycle {
    ignore_changes = [ebs_optimized]
  }
}