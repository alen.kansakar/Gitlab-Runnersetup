terraform {
  required_version = ">= 0.12"
  # User only after bucket is created and bucket name is placed
  # backend "s3" {
  #   bucket = "<Name replace>" 
  #   key    = "tfstate"
  #   region = "us-west-2"
  # }
}
provider "aws" {
  version = "~> 2.31.0"
  region  = "us-west-2"
}
data "aws_caller_identity" "current" {}
resource "aws_s3_bucket" "b" {
  bucket = "mystatebucket2${data.aws_caller_identity.current.account_id}"
  acl    = "private"

  tags = {
    Name    = "Terraform Test State file Storage"
    Purpose = "To add state"
  }
}